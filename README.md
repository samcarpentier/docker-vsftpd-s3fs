# docker-alpine-s3fs

Uses [s3fs](https://github.com/s3fs-fuse/s3fs-fuse) with Alpine Linux to mount an S3 bucket as a directory in the filesystem. Exposes files with VSFTPD.

## Configuration

#### S3FS

* S3_ACCESS_KEY
* S3_SECRET_KEY
* S3_URL
* S3_BUCKET
* MOUNTPOINT (defaults to `/mnt/s3fs/${S3_BUCKET}`)

#### VSFTPD

* FTP_USER
* FTP_PASS
* VSFTPD_* (e.g. VSFTPD_PASV_MIN_PORT, VSFTPD_PASV_MAX_PORT, VSFTPD_PASV_ADDRESS, VSFTPD_PASV_ADDR_RESOLVE, ...)
* MOUNTPOINT_DEFAULT_ACL (defaults to `public-read`)

## Running with Docker

```bash
docker run -ti \
  -e S3_ACCESS_KEY=access_key \
  -e S3_SECRET_KEY=secret_key \
  -e S3_URL=https://s3.example.com:6443 \
  -e S3_BUCKET=my-bucket \
  -e FTP_USER=test \
  -e FTP_PASS=test \
  -e VSFTPD_PASV_MIN_PORT=21100 \
  -e VSFTPD_PASV_MAX_PORT=21110 \
  -e VSFTPD_PASV_ADDRESS=ftp.example.com \
  -e VSFTPD_PASV_ADDR_RESOLVE=YES \
  -e MOUNTPOINT_DEFAULT_ACL=private \
  --privileged \
  samcarpentier/vsftpd-s3fs:alpine
```

**Note: Docker privileged mode is required. Otherwise, the following error will occur.**

```
fuse: device not found, try 'modprobe fuse' first
```
